# See https://gitlab.com/gitlab-org/gitlab-ce/issues/55442
task :default => :test

desc "Run unit tests."
task :test do
  ruby "test.rb"
end
